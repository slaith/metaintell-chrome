// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });


//example of using a message handler from the inject scripts
token=null;
api="https://api.metaintell.com/public/api.php";
function authenticateClient(){
	var data={
		call:"register_client",
		params:JSON.stringify({guid:"17084b40-08f5-4bcd-a739-c0d08c176bad"})
	}
	$.post(api,data, function(response){token=response.api_token},"json");
}
authenticateClient();
function getPackageDetails(packagename){
	//Make an xhr call
	var data={
		call:"get_score",
		api_token:token,
		params:JSON.stringify({packages:[packagename]})
	}
	var result;
	$.ajax({type:"GET",async:false,url:api,data:data, success:function(response){result=response},dataType:"json"});
	//console.log(result);

	var returnObj= new Object();
	if(result.result[0].privacy_risk){
		returnObj.result=result.result[0];
		returnObj.found=true;
		//console.log("Returning object: ",returnObj)

	}
	return returnObj;
}

function getMultiPackages(packagenames){
	//Make an xhr call
	var data={
		call:"get_score",
		api_token:token,
		params:JSON.stringify({packages:packagenames})
	}
	var result;
	$.ajax({type:"GET",async:false,url:api,data:data, success:function(response){result=response},dataType:"json"});
	
	//console.log(result);
	return result;
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
 
  if(request.action=="packagedetails")
  {
  	sendResponse( getPackageDetails(request.package));
  }

   if(request.action=="multiPackages")
  {
  	var packagedArray=new Object();
  	var thePackages= getMultiPackages(request.packages);
  	console.log("The multipackages received from metaintell:",thePackages)
  	for(i in thePackages.result){
  		var p=thePackages.result[i];
  		packagedArray[p.package]=p;

  	}
  	sendResponse(packagedArray);
  }
  //	chrome.pageAction.show(sender.tab.id);
    //sendResponse();
    return true;
  });;